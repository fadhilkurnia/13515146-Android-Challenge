# Shopping List

A simple Android Application to learn activity state and implicit intent. Also using Butterknife for annotation, and RecyclerView for showing Product data.

## Some Screenshoots

![Alt text](https://image.ibb.co/hjoUpF/Shopping_List1.jpg "Choose product to buy")
![Alt text](https://image.ibb.co/ewk9pF/Shopping_List2.jpg "Products on your cart")
![Alt text](https://image.ibb.co/hkVs9F/Whats_App_Image_2017_09_04_at_8_44_33_PM.jpg "Empty state")