package com.fadhilimam.shoppinglist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private boolean showAddButton;
    private List<Product> data;
    private buttonClickListener listener;

    ProductAdapter(List<Product> data, buttonClickListener listener, boolean showAddButton) {
        this.data = data;
        this.listener = listener;
        this.showAddButton = showAddButton;
    }

    ProductAdapter(List<Product> data) {
        this.data = data;
        this.listener = listener;
        this.showAddButton = false;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_add_item, parent, false);

        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        final Product product = data.get(position);
        holder.productName.setText(product.getName());
        holder.productImage.setImageResource(product.getImageRes());
        if (!showAddButton) {
            holder.addButton.setVisibility(View.GONE);
        } else  {
            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onButtonClickListener(product);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView productName;
        ImageButton addButton;

        ProductViewHolder(View itemView) {
            super(itemView);

            productImage = itemView.findViewById(R.id.product_image);
            productName = itemView.findViewById(R.id.product_name);
            addButton = itemView.findViewById(R.id.button_add);

        }
    }

    interface buttonClickListener {
        void onButtonClickListener(Product data);
    }

}
