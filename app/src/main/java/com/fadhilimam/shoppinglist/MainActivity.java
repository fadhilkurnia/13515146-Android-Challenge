package com.fadhilimam.shoppinglist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    public static final int PRODUCT_REQUEST = 1;
    private ArrayList<Product> shoppingItem = new ArrayList<>();
    private ProductAdapter adapter;

    @BindView(R.id.shopping_list) protected RecyclerView shoppingList;
    @BindView(R.id.empty_state) protected LinearLayout emptyState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean("data_exist")) {
                ArrayList<Integer> imageData = savedInstanceState.getIntegerArrayList("image_data");
                ArrayList<String> nameData = savedInstanceState.getStringArrayList("name_data");

                if (nameData != null && imageData != null) {
                    for (int i = 0; i < nameData.size(); i++) {
                        shoppingItem.add(new Product(imageData.get(i), nameData.get(i)));
                    }
                }

            }
        }

        if (shoppingItem.size() > 0) {
            emptyState.setVisibility(View.GONE);
        }

        initializeList();

    }

    public void addProduct(View view) {
        Intent intent = new Intent(this, ProductActivity.class);
        startActivityForResult(intent, PRODUCT_REQUEST);
    }

    public void locateShop(View view) {
        Uri addressUri = Uri.parse("geo:0,0?q=" + getString(R.string.shop_name));
        Intent locateIntent = new Intent(Intent.ACTION_VIEW, addressUri);

        if (locateIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(locateIntent);
        } else {
            Log.i(LOG_TAG, getString(R.string.can_not_open_map));
            Toast.makeText(this, R.string.can_not_open_map, Toast.LENGTH_SHORT).show();
        }

    }

    private void initializeList() {
        adapter = new ProductAdapter(shoppingItem);
        shoppingList.setLayoutManager(new LinearLayoutManager(this));
        shoppingList.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PRODUCT_REQUEST && resultCode == RESULT_OK) {
            String productName = data.getStringExtra(ProductActivity.EXTRA_PRODUCT_NAME);
            int produtImageRes = data.getIntExtra(ProductActivity.EXTRA_PRODUCT_IMAGE, R.mipmap.ic_launcher);
            Product product = new Product(produtImageRes, productName);
            shoppingItem.add(product);
            adapter.notifyDataSetChanged();
            if (shoppingItem.size() > 0) {
                emptyState.setVisibility(View.GONE);
            }
            Toast.makeText(this, "Adding " + productName + " into cart", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<Integer> imageData = new ArrayList<>();
        ArrayList<String> nameData = new ArrayList<>();
        for (Product product : shoppingItem) {
            imageData.add(product.getImageRes());
            nameData.add(product.getName());
        }

        outState.putBoolean("data_exist", shoppingItem.size() > 0);
        outState.putIntegerArrayList("image_data", imageData);
        outState.putStringArrayList("name_data", nameData);

    }
}
